#include "tictactoe.h"
#include <QtGui/QApplication>
#include<QTextCodec>
int main(int argc, char *argv[])
{
	QTextCodec::setCodecForCStrings(QTextCodec::codecForName("CP-1251"));
	QTextCodec::setCodecForLocale(QTextCodec::codecForName("CP-1251"));
	QTextCodec::setCodecForTr(QTextCodec::codecForName("CP-1251"));

	QApplication a(argc, argv);
	tictactoe w;
	w.show();
	return a.exec();
}
