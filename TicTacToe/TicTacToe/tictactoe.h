#ifndef TICTACTOE_H
#define TICTACTOE_H

#include <QtGui/QMainWindow>
#include "ui_tictactoe.h"

class tictactoe : public QMainWindow
{
	Q_OBJECT

public:
	tictactoe(QWidget *parent = 0, Qt::WFlags flags = 0);
	~tictactoe();

private:
	Ui::tictactoeClass ui;

	int field[9][9];//������ ������� ������� �� ������� ����. -1 - ��� �������, 1 - ������� ������� ������, 2 - ������� ������� ������. ������ ������� ������� ������������� ������ �������� ����
	int player;//����� ������, ������� ��������� ���
	int winner;//����� ����������
	int player2Score;//���� ����� ������� ������-��������
	int aiScore;//���� ����� ��
	int gameMod;//��������� ��������, 0 - �������, 1 - ��
	int gameTheme;//����� ��������� �������� ����� �������� ����
	int turnCount;//���������� ����������� ����� ����� ��������
	bool gameFinished;//���� ��������� ����
	bool gameFinishedNoWinner;//���� ��������� ���� � �����
	bool showMessage;//���� ������ ���������-��������������.

	/*!
	*	������ ����� ����
	*/
	void startNewGame();

	/*!
	*	����������� ������� ������ �� ������� ����
	*	\param[in] i - ����� ������ ������
	*	\param[in] j - ����� ������� ������
	*/
	void changeChosenCell(int i, int j);

	/*!
	*	�������� �� ��������� ����
	*	\param[in] i - ����� ������ ��������� ����������� ������
	*	\param[in] j - ����� ������� ��������� ����������� ������
	*	\return - ������� ����� ����. True - ���� �����������, ����� ���� ������������
	*/
	bool gameIsFinished(int i, int j);

	/*!
	*	�������� ���� ������� ������
	*	\param [in|out] player - ��������� �� ����� ������, ������� �������� ���
	*/
	void changePlayer(int* player);

	/*!
	*	��� ��. ������� �����������, ���� ������� ����� ���� � ��, ����� ���� ������� ������
	*	\param [in|out] x - ��������� �� ����� ������ ������, � ������� �� �������� �������. [0, 8]
	*	\param [in|out] y - ��������� �� ����� ������� ������, � ������� �� �������� �������. [0, 8]
	*/
	void aiTurn(int* x, int* y);

	/*!
	*	����� ����� ����� ������� �� �����
	*/
	void setScores();

	/*!
	*	����� ������ ����� �������� ���� � ������ �� ��������� � ���������
	*	\param [in|out] allTurns - ������ �� ���������, � ������� ��������� ������ ���������
	*	\param [in] size - ������ �������� ����
	*/
	void findEmptyCells(QVector<QPoint>& allTurns, int size);

	/*!
	*	����� ����� �������� ����, ������� �������� ����������� ��� ������� ������, � ������ �� ��������� � ���������
	*	\param [in|out] playerWin - ������ �� ���������, � ������� ��������� ������ ���������
	*	\param [in] size - ������ �������� ����
	*/
	void findCells1stPlayerWin(QVector<QPoint>& playerWin, int size);

	/*!
	*	����� ����� �������� ����, ������� �������� ����������� ��� ������� ������, � ������ �� ��������� � ���������
	*	\param [in|out] aiWin - ������ �� ���������, � ������� ��������� ������ ���������
	*	\param [in] size - ������ �������� ����
	*/
	void findCells2ndPlayerWin(QVector<QPoint>& aiWin, int size);


private slots:

	/*!
	*	��� ������ ��� ������� �� ������ �������� ����. ���� ������� ��, �� �� ����� ����� ���� ��������� �� ������ ������
	*/
	void onCellChosen();

	/*!
	*	����� �������� ����� �������� ����
	*/
	void onThemeChanged();

	/*!
	*	����� ����������
	*/
	void onPlayer2Changed();

	/*!
	*	����� ������� �������� ����
	*/
	void onSizeChanged();
};

#endif // TICTACTOE_H
