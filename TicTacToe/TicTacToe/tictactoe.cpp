#include "tictactoe.h"
#include<QPoint>
#include<QVector>
#include<cstdlib>
#include<ctime>
#include<QMessageBox>
#include<QMouseEvent>


tictactoe::tictactoe(QWidget *parent, Qt::WFlags flags)
	: QMainWindow(parent, flags)
{
	ui.setupUi(this);

	srand(time(NULL));
	
	gameTheme=1;//������� ������ �������� �����
	gameMod=0;//������� ���������-��������
	showMessage=true;//���������� ���� ������ ���������-��������������
	player2Score=0;//�������� ���� ������� ������-��������
	aiScore=0;//�������� ���� ������� ������-��
	startNewGame();//������ ����� ����
	
	//����������� ��������
	connect(ui.scene, SIGNAL(cellClicked(int, int)), this, SLOT(onCellChosen()));
	connect(ui.fieldSize, SIGNAL(valueChanged(int)), this, SLOT(onSizeChanged()));
	connect(ui.theme, SIGNAL(currentIndexChanged(int)), this, SLOT(onThemeChanged()));
	connect(ui.player, SIGNAL(currentIndexChanged(int)), this, SLOT(onPlayer2Changed()));
}

tictactoe::~tictactoe()
{

}

void tictactoe::onCellChosen()
{
	int i, j;//���������� ��������� ������ �������� ����
	i=ui.scene->currentRow();//���������� ������ ��������� ������
	j=ui.scene->currentColumn();//���������� ������� ��������� ������
	
	
	if(field[i][j]==-1)
	{//���� ������ ������
		field[i][j]=player;//��������� ������� �������� ������ � ������� �������� ����
		changeChosenCell(i, j);//��������� ������� ������ �� ������� ���� � ��������� ������ 
		gameFinished=gameIsFinished(i, j);//��������� ������ �������� ������
		if(gameFinished && !gameFinishedNoWinner)
		{//���� ���� ����������� � ���� ����������
			winner=player;//������� ����������� �������� ������
			if(winner==2)//���� ������� ������ �����, �� ��������� ���� ����� ������� ������ �� 1
				player2Score++;
		}
		changePlayer(&player);//�������� ��� ������� ������
		if(gameMod && !gameFinished)
		{//���� ������ ��������-�� � ���� �� �����������
			aiTurn(&i, &j);
			gameFinished=gameIsFinished(i, j);
			changePlayer(&player);//�������� ��� ������� ������
			if(gameFinished && !gameFinishedNoWinner)
			{//���� ���� ����������� � ���� ����������		
				winner=2;//������� ����������� ������� ������
				aiScore++;//��������� ���� ����� �� �� 1
			}
		}
		if(gameFinished)
		{//���� ���� �����������
			if(!gameFinishedNoWinner)//���� ���� ����������
				setScores();//������� ���� ����� ������� �� �����
			startNewGame();//������ ����� ����
		}
	}
}

void tictactoe::onThemeChanged()
{
	
	QMessageBox::StandardButton button;//��������� � ��������
	if(showMessage && turnCount!=0)//���� ��������� ���� ������ ��������� � ���������� ����� �� ����� 0
		//������� ���������-�������������� � ������ ������� ����
		button=QMessageBox::warning(this, tr(""),tr("���� ������� ����, ������� ���� ���������. ������ ����������?"),
			QMessageBox::Yes | QMessageBox::Cancel);
	if(button==QMessageBox::Yes || turnCount==0)
	{//���� ���� ������ ������ "��" ��� ���������� ����� ������� ����� 0 
		gameTheme=ui.theme->currentIndex()+1;//�������� ����� ��������� �������� �����
		startNewGame();//������ ����� ����
	}
	else
	{//���� ���� ������ ������ ������
		showMessage=false;//����� ���� ������ ���������-��������������
		ui.theme->setCurrentIndex(gameTheme-1);//���������� ������� �������� �����
		showMessage=true;//��������� ���� ������ �������� ���������
	}
}

void tictactoe::onPlayer2Changed()
{
	QMessageBox::StandardButton button;//��������� � ��������
	if(showMessage && turnCount!=0)//���� ��������� ���� ������ ��������� � ���������� ����� �� ����� 0
		//������� ���������-�������������� � ������ ������� ����
		button=QMessageBox::warning(this, tr(""),tr("���� ������� ���������, ������� ���� ���������. ������ ����������?"),
			QMessageBox::Yes | QMessageBox::Cancel);
	if(button==QMessageBox::Yes || turnCount==0)
	{//���� ���� ������ ������ "��" ��� ���������� ����� ������� ����� 0 
		gameMod=ui.player->currentIndex();//������� ������ ���������
		if(!gameMod)//���� ������ ��������-�������
			ui.score2->display(player2Score);//������� ���� ����� ������� ������-��������
		else
			ui.score2->display(aiScore);//������� ���� ����� ��
		
		startNewGame();//������ ����� ����
	}
	else
	{//���� ���� ������ ������ ������
		showMessage=false;//����� ���� ������ ���������-��������������
		ui.player->setCurrentIndex(gameMod);//���������� �������� ���������
		showMessage=true;//��������� ���� ������ �������� ���������
	}
}

void tictactoe::onSizeChanged()
{
	QMessageBox::StandardButton button;//��������� � ��������
	if(showMessage && turnCount!=0)//���� ��������� ���� ������ ��������� � ���������� ����� �� ����� 0
		//������� ���������-�������������� � ������ ������� ����
		button=QMessageBox::warning(this, tr(""),tr("���� �������� ������ ����, ������� ���� ���������. ������ ����������?"),
			QMessageBox::Yes | QMessageBox::Cancel);
	if(button==QMessageBox::Yes || turnCount==0)
	{//���� ���� ������ ������ "��" ��� ���������� ����� ������� ����� 0 
		startNewGame();//������ ����� ����
	}
	else
	{//���� ���� ������ ������ ������
		showMessage=false;//����� ���� ������ ���������-��������������
		ui.fieldSize->setValue(ui.scene->rowCount());//���������� �������� ���������
		showMessage=true;//��������� ���� ������ �������� ���������
	}
}

void tictactoe::startNewGame()
{	
	int size=ui.fieldSize->value();//������ �������� ����
	QString brushName, theme;//�������� ����� �������� �����
	brushName="pole"+theme.setNum(gameTheme)+".jpg";//������� �������� �������� �����
	QBrush brush(QColor(255,255,255), QPixmap(brushName).scaled(380/size, 380/size));//������� ����� ��� ������� �����
	ui.scene->clear();//�������� ������� ����
	//������ ������ �������� ����
	ui.scene->setRowCount(size);
	ui.scene->setColumnCount(size);
	QTableWidgetItem* item;//��������� �� ������� �������
	//��� ������� ���� �������� ����
	for (int i=0; i<size; i++)
	{
		ui.scene->setRowHeight(i, 380/size);//������ ������ ������
		// ��� ������� ������� �������� ����
		for(int j=0; j<size; j++)
		{
			ui.scene->setColumnWidth(j, 380/size);//������ ������ �������
			item=new QTableWidgetItem;
			ui.scene->setItem(i, j, item);//��������� ������ �������
			ui.scene->item(i, j)->setBackground(brush);//�������� ������ ������ �������
		}
	}
	
	ui.playerNum->setText("1");//��������� ������� ������ � �������� ����
	//�������� ������ �������� ���� �� ������� �������
	for(int i=0; i<9; i++)
		for(int j=0; j<9; j++)
			field[i][j]=-1;
	player=1;//��������� ����� �������� ������ ������ 1
	turnCount=0;//��������� ���������� �����, ����������� ��������, ������ 0
	gameFinished=false;//����� ���� ����� ����
	gameFinishedNoWinner=false;//����� ���� ����� ���� � �����
}

void tictactoe::changeChosenCell(int i, int j)
{
	QString brushName, theme;//�������� �������� ������� �������
	int size=ui.fieldSize->value();//������ �������� ����
	brushName="model"+theme.setNum(gameTheme);//������� �������� �������� ������� �������
	if(player==1)//���� ������� ��� ������� ������
		brushName+="x.png";//������� ������� "�"
	else
		brushName+="o.png";//������� ������� "�"
	QBrush brush(QColor(255,255,255), QPixmap(brushName).scaled(380/size, 380/size));//������� ����� ��� ������� ������
	ui.scene->item(i, j)->setBackground(brush);//��������� ������� � ������
}

bool tictactoe::gameIsFinished(int i, int j)
{
	turnCount++;//��������� ����� ����� ������� �� 1
	int x, y;//���������� ������, � ������� ��������� ��������� �������
	int matchCountVertical=0;//���������� ���������� ������� ������� ������ �� ���������
	int	matchCountHorisontal=0;//���������� ���������� ������� ������� ������ �� �����������
	int	matchCountMajorDiagonal=0;//���������� ���������� ������� ������� ������ �� ������� ���������
	int	matchCountMinorDiagonal=0;//���������� ���������� ������� ������� ������ �� �������� ���������
	int size=ui.fieldSize->value();//������ �������� ����
	bool isFinish=true;//���� ����� ����
	//��������� ���������� ��������� ������
	x=i;
	y=j;
	//����� ���������� �� ���������
	/*������ ��� ������ �� ��������� ���� �� ��� ���, ���� ������� ������� � ������� 
	� ������� �������� ������ ���������, ���������� ���������� ���������� ������� �������
	*/
	while(field[x][j]==player && x<size)
	{
		matchCountVertical++;
		x++;
	}
	//��������� ����� ������ ��������� ������
	x=i;
	/*������ ��� ������ �� ��������� ����� �� ��� ���, ���� ������� ������� � ������� 
	� ������� �������� ������ ���������, ���������� ���������� ���������� ������� �������
	*/
	while(field[x][j]==player && x>=0)
	{
		matchCountVertical++;
		x--;
	}
	matchCountVertical--;

	//����� ���������� �� �����������
	/*������ ��� ������ �� ����������� ������ �� ��� ���, ���� ������� ������� � ������� 
	� ������� �������� ������ ���������, ���������� ���������� ���������� ������� �������
	*/
	while(field[i][y]==player && y<size)
	{
		matchCountHorisontal++;
		y++;
	}
	//��������� ����� ������� ��������� ������
	y=j;
	/*������ ��� ������ �� ����������� ����� �� ��� ���, ���� ������� ������� � ������� 
	� ������� �������� ������ ���������, ���������� ���������� ���������� ������� �������
	*/
	while(field[i][y]==player && y>=0)
	{
		matchCountHorisontal++;
		y--;
	}
	matchCountHorisontal--;
	//����� ���������� �� ������� ���������
	//��������� ������ ���� � ������� ��������� ������
	x=i;
	y=j;
	/*������ ��� ������ �� ��������� ����� ����� �� ��� ���, ���� ������� ������� � ������� 
	� ������� �������� ������ ���������, ���������� ���������� ���������� ������� �������
	*/
	while(field[x][y]==player && y>=0 && x>=0)
	{
		matchCountMajorDiagonal++;
		y--;
		x--;
	}
	//��������� ������ ���� � ������� ��������� ������
	x=i;
	y=j;
	/*������ ��� ������ �� ��������� ������ ���� �� ��� ���, ���� ������� ������� � ������� 
	� ������� �������� ������ ���������, ���������� ���������� ���������� ������� �������
	*/
	while(field[x][y]==player && y<size && x<size)
	{
		matchCountMajorDiagonal++;
		y++;
		x++;
	}
	matchCountMajorDiagonal--;
	//����� ���������� �� �������� ���������
	//��������� ������ ���� � ������� ��������� ������
	x=i;
	y=j;
	/*������ ��� ������ �� ��������� ����� ���� �� ��� ���, ���� ������� ������� � ������� 
	� ������� �������� ������ ���������, ���������� ���������� ���������� ������� �������
	*/
	while(field[x][y]==player && y>=0 && x<size)
	{
		matchCountMinorDiagonal++;
		y--;
		x++;
	}
	//��������� ������ ���� � ������� ��������� ������
	x=i;
	y=j;
	/*������ ��� ������ �� ��������� ������ ����� �� ��� ���, ���� ������� ������� � ������� 
	� ������� �������� ������ ���������, ���������� ���������� ���������� ������� �������
	*/
	while(field[x][y]==player && y<size && x>=0)
	{
		matchCountMinorDiagonal++;
		y++;
		x--;
	}
	matchCountMinorDiagonal--;//��������� �������� �� �������� ����������
	
	if(matchCountMinorDiagonal==size || matchCountMinorDiagonal>=5 ||	
		matchCountMajorDiagonal==size || matchCountMajorDiagonal>=5 || 
		matchCountVertical==size || matchCountVertical>=5 || 
		matchCountHorisontal==size || matchCountHorisontal>=5)//���� �� ����������� ��� �� ���������, ��� �� ������� ��������� ��� �� �������� ��������� ���� ����������, ���-�� ������� ����� ������� ���� ��� ��� ������ ��� ����� ����
		return true;//������� ������� ����� ����
		
	if(turnCount==size*size)
	{//���� ����� �����, ����������� ����� ������, ����� ���������� ������ �������� ����
			//������� ��������� � ���, ��� ���� ����������� � �����
		QMessageBox messageNoWinner("�����!", "�������� �����������",QMessageBox::NoIcon,QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		messageNoWinner.setIconPixmap(QPixmap("icon.png"));
		messageNoWinner.show();
		int shown=messageNoWinner.exec();

		gameFinishedNoWinner=true;//��������� ���� ����� ���� � �����
		return true;//������� ������� ����� ����
	}
	return false;
}

void tictactoe::changePlayer(int* player)
{
	if(*player==1)
	{//���� ������� ��� �������� ������ �����
		*player=2;//������� ������� ������� ������� ������
		ui.playerNum->setText("2");//��������� � �������� ������� �������� ���� ������� ������
	}
	else
	{
		*player=1;//������� ������� ������� ������� ������
		ui.playerNum->setText("1");//��������� � �������� ������� �������� ���� ������� ������
	}
}

void tictactoe::aiTurn(int* x, int* y)
{
	int size=ui.fieldSize->value();
	QPoint turn(0,0);//��������� ������ ���� ��
	QVector<QPoint>	playerWin;//��������� ���������� ��������� ���� ������� ������
	QVector<QPoint> aiWin;//��������� ���������� ��������� ���� ������� ������
	QVector<QPoint> allTurns;//��������� ������ �����
	//�������� ��� ��������� ���� � ��������� ������ �����
	findEmptyCells(allTurns, size);
	//��������� ������ 1 ������ �� ��������� ����
	findCells1stPlayerWin(playerWin, size);
	//��������� ������ �� �� ������� ����
	findCells2ndPlayerWin(aiWin, size);
	//����� ���� ��
	if (!aiWin.isEmpty())//e��� �� ����� ��������
		turn+=aiWin[rand()%aiWin.size()];//������� ��������� ���������� ������ �� ���������� �������� ����� ��
	else
		if(!playerWin.isEmpty())//���� ����� ����� ��������
			turn+=playerWin[rand()%playerWin.size()];//������� ��������� ���������� ������ �� ���������� �������� ����� ������� ������
		else//���� ����� �� ������ ���� �������������� �� �������
			turn+=allTurns[rand()%allTurns.size()];//������� ��������� ���������� ������ �� ���������� ������ ������
	field[turn.x()][turn.y()]=2;//������� ������ ������� �������� ���� ������� ������� ������
			
		changeChosenCell(turn.x(),turn.y());//���������� ������� ������� ������� ������ �� ��������� ������
	//return gameIsFinished(turn.x(),turn.y());//������� ������� ����� ��� ����������� ����
		*x=turn.x();
		*y=turn.y();
}

void tictactoe::setScores()
{
	//��������� � ������ ������� ������
	QMessageBox messageWinner2ndPlayer("������!", "������� ������ �����",QMessageBox::Information,QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
	//��������� � ������ ������� ������
	QMessageBox messageWinner1stPlayer("������!", "������� ������ �����",QMessageBox::Information,QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
	messageWinner1stPlayer.setIconPixmap(QPixmap("icon.png"));
	messageWinner2ndPlayer.setIconPixmap(QPixmap("icon.png"));
	int shown;
	if(winner==1)
	{//���� ������� ������ ����� 
		ui.score1->display((ui.score1->intValue())+1);//��������� ������� ����� ������� ������ � ������� ���� �� 1
		messageWinner1stPlayer.show();//������� ��������� � ������ 1 ������
		shown = messageWinner1stPlayer.exec();
	}
	else
	{//���� ������� ������ ����� 
		if(gameMod==0)//���� ������ ����� �������� ���������
			ui.score2->display(player2Score);//������� � ������� ����� ������� ������ � ������� ���� ���������� ����� ���������-��������
		else	//���� ������ ����� �������� ��
			ui.score2->display(aiScore);//������� � ������� ����� ������� ������ � ������� ���� ���������� ����� ��
		messageWinner2ndPlayer.show();
		shown = messageWinner2ndPlayer.exec();
	}
}

void tictactoe::findEmptyCells(QVector<QPoint>& allTurns, int size)
{
	//��� ���� �����
	for (int i=0;i<size;i++)
		for(int j=0; j<size;j++)
			if(field[i][j]==-1)//���� ������ ������
				allTurns.append(QPoint(i,j));//�������� �� ���������� � ���������
}

void tictactoe::findCells1stPlayerWin(QVector<QPoint>& playerWin, int size)
{
	//��� ���� ������ �������� ����
	for (int i=0;i<size;i++)
		for(int j=0; j<size;j++)
			if(field[i][j]==-1)
			{	//���� ������ ������
				turnCount--;//��������� ����� ����� �� 1
				field[i][j]=1;//�������������� ������� � ������ ������� ������� ������
				player=1;//��������� �������������� ������� ������� ������� ������
				if(gameIsFinished(i,j))//���� ������ ����� ����� ��������
					playerWin.append(QPoint(i,j));//�������� ���������� ������ � ��������� �������� ����� ������� ������
				//������� ������� �������� ������ � �������� ������
				field[i][j]=-1;
				player=2;
			}
}

void tictactoe::findCells2ndPlayerWin(QVector<QPoint>& aiWin, int size)
{
	//��� ���� ������ �������� ����
	for (int i=0;i<size;i++)
		for(int j=0; j<size;j++)
			if(field[i][j]==-1)
			{	//���� ������ ������
				turnCount--;//��������� ����� ����� �� 1
				field[i][j]=2;//�������������� ������� � ������ ������� ������� ������
				if(gameIsFinished(i,j))//���� ������ ����� ����� ��������
					aiWin.append(QPoint(i,j));//�������� ���������� ������ � ��������� �������� ����� ��
				field[i][j]=-1;//������� ������� �������� ������ �������� ����
			}
}
